use std::fmt;
use std::fs::File;
use std::io::{BufReader, Write};
use std::collections::HashMap;
use serde::{Serialize, Deserialize};
use serde_json::json;

pub enum Error {
    EmailExists,
    InputNotEmail,
    AuthFailed,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &*self {
            Error::EmailExists => write!(f, "User with that email already exists!"),
            Error::InputNotEmail => write!(f, "Not an email!"),
            Error::AuthFailed => write!(f, "Login failed!")
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct User {
   pub(crate) email: String, 
   pub(crate) full_name: Option<String>,
   #[serde(skip_serializing)]
   pub(crate) password: String,
}

impl User {
    pub(crate) fn new(email: &String, password: &String) -> Self {
        Self {
            email: email.clone(),
            full_name: None,
            password: password.clone()
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct Users {
    users: HashMap<String, User>,
}

impl Users {
    fn new() -> Self {
        Self { users: HashMap::new() }
    }

    pub fn push(&mut self, email: &String, password: &String) -> Result<(), Error> {
        let e:Vec<&str> = email.split("@").collect();
        if e.len() != 2 {
            return Err(Error::InputNotEmail);
        }
        if self.users.contains_key(email) {
            return Err(Error::EmailExists);
        }

        self.users.insert(email.clone(), User::new(&email, &password));
        self.save();
        Ok(())
    }

    pub fn login(&self, email: &str, pw: &str) -> Result<(), Error> {
        if let Some(user) = self.users.get(email) {
            if user.password == pw {
                return Ok(());
            }
        }

        return Err(Error::AuthFailed);
    }

    fn save(&self) {
        let mut filename = dirs::config_dir().unwrap();
        filename.push("homecloud/users.json");
        match File::create(&filename) {
            Ok(mut file) => {
                let mut js = json!(&self);
                // not auto serialized so we have to add it.
                for (email, user) in &self.users {
                    js["users"][email]["password"] = json!(user.password.clone());
                }

                if let Err(err) = file.write(serde_json::to_string(&js).unwrap_or(format!("")).as_bytes()) {
                    eprintln!("{} on '{:?}'", err, filename);
                }
            }
            Err(err) => {
                eprintln!("{} on '{:?}'", err, filename);
            }
        }
    }

    pub fn load() -> Self {
        let mut filename = dirs::config_dir().unwrap();
        filename.push("homecloud/users.json");
        match File::open(filename) {
            Ok(file) => {
                let buf = BufReader::new(file);
                let users: Users = match serde_json::from_reader(buf) {
                    Ok(users) => {
                        users
                    }
                    Err(e) => {
                        eprintln!("Could not read users.json got {}", e);
                        Users::new()
                    }
                };
                users
            }
            Err(e) => {
                eprintln!("Could not open users.json got {} Make sure you have created it.", e);
                Users::new()
            }
        }
    }
}
