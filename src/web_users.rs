use std::fs::File;
use std::io::{Read, BufReader};
use std::sync::{Arc, Mutex};
use crate::users::{User, Users};
use crate::JsonResponse;
use actix_web::{web, HttpResponse};
use actix_identity::Identity;
use serde::Deserialize;

pub async fn login_form() -> HttpResponse {
    login_form_redirect("/").await
}

pub async fn login_form_redirect(redirect: &str) -> HttpResponse {
    let mut body = String::new();
    match File::open("templates/login.html") {
        Ok(file) => {
            let mut reader = BufReader::new(file);
            reader.read_to_string(&mut body).unwrap();
            body = body.replace("{{REDIRECT}}", redirect);
        }
        Err(e) => {
            eprintln!("{} login.html", e);
        }
    }
    HttpResponse::Ok()
        .content_type("text/html; charset: utf-8")
        .body(body)
}

pub async fn add_form() -> HttpResponse {
    let mut body = Vec::new();
    match File::open("templates/user_add.html") {
        Ok(mut file) => {
            file.read_to_end(&mut body).unwrap();
        }
        Err(e) => {
            eprintln!("{} user_add.html", e);
        }
    }
    HttpResponse::Ok()
        .content_type("text/html; charset: utf-8")
        .body(body)
}

pub async fn add_submit(form: web::Json<User>, users: web::Data<Arc<Mutex<Users>>>) -> HttpResponse {
    if let Err(e) = (*users.lock().unwrap()).push(&form.email, &form.password) {
        return HttpResponse::Ok().json(JsonResponse::new(&format!("{}", e), ""));
    }

    return HttpResponse::Ok().json(JsonResponse::new("User has been added", "/user/login"));
}

#[derive(Deserialize)]
pub struct UserForm {
    url: String,
    email: String,
    password: String,
}

pub async fn login_submit(user: web::Json<UserForm>, id: Identity, users: web::Data<Arc<Mutex<Users>>>) -> HttpResponse {
    if let Err(_) = (*users.lock().unwrap()).login(&user.email, &user.password) {
//        return HttpResponse::Ok().json(JsonResponse::new(&format!("{}", e), ""));
    }
    id.remember(user.email.clone());
    return HttpResponse::Ok().json(JsonResponse::new("Logged in", &user.url));
}

