mod users;
mod web_users;
mod web_albums;
mod mediacrawler;
use std::sync::{Arc, Mutex};
use std::sync::mpsc::channel;
use std::fs::File;
use std::io::{Read, BufReader};
use std::path::PathBuf;
use std::net::{SocketAddr, IpAddr};
use futures::{StreamExt};
use actix_web::middleware::Logger;
use actix_identity::Identity;
use actix_identity::{CookieIdentityPolicy, IdentityService};
//use actix_web::middleware::errhandlers::{ErrorHandlerResponse, ErrorHandlers};
use env_logger;
use actix_web::{web, App, HttpResponse, HttpServer};
use rustls::internal::pemfile::{certs, rsa_private_keys};
use rustls::{NoClientAuth, ServerConfig};
use structopt::StructOpt;
//use webdav_handler::{fakels::FakeLs, localfs::LocalFs, DavHandler};
use mediacrawler::Albums;
use users::{Users};

#[derive(StructOpt)]
struct Args {
    #[structopt(short="k", long="keys", parse(try_from_str = path_to_buf_reader), help="Key file (pem format)")]
    keys: Option<BufReader<File>>,
    #[structopt(short="c", long="cert", parse(try_from_str = path_to_buf_reader), help="Cert file (pem format)")]
    cert: Option<BufReader<File>>,
    #[structopt(short="m", long="media", help="Path to images.")]
    path: PathBuf,
    #[structopt(short="i", long="interface", help="Network interface to listen on.")]
    interface: Option<String>,
    #[structopt(short="l", long="listen", help="Ip adress to listen.")]
    address: Option<IpAddr>,
    #[structopt(short="p", long="port", help="TCP Port to listen on.", default_value="8080")]
    port: u16,
}

fn path_to_buf_reader(path: &str) -> Result<BufReader<File>, &'static str> {
	match File::open(path) {
		Ok(file) => {
			Ok(BufReader::new(file))
		}
		Err(err) => {
			eprintln!("{}", err);
			Err("error argument file error")
		}
	}
}

async fn stylesheet() -> HttpResponse {
    let mut body = Vec::new();
    match File::open("templates/stylesheet.css") {
        Ok(mut file) => {
            file.read_to_end(&mut body).unwrap();
        }
        Err(e) => {
            eprintln!("{}", e);
        }
    }
    HttpResponse::Ok()
        .content_type("text/css")
        .body(body)
}

#[derive(serde::Serialize)]
struct JsonResponse {
    msg: String,
    url: String,
}
impl JsonResponse {
    fn new(msg: &str, url: &str) -> Self {
        Self {
            msg: msg.into(),
            url: url.into()
        }
    }
}

async fn debug(mut req: web::Payload, users: web::Data<Arc<Mutex<Users>>>) -> HttpResponse {
    println!("{:?}", req.next().await);
    return HttpResponse::Ok().json(JsonResponse::new("no", ""));
}

async fn index(id: Identity) -> HttpResponse {
    if id.identity().is_none() {
        return web_users::login_form_redirect("/").await;
    }
    let mut body = Vec::new();
    match File::open("templates/image_page.html") {
        Ok(mut file) => {
            file.read_to_end(&mut body).unwrap();
        }
        Err(e) => {
            eprintln!("{} image_page.html", e);
        }
    }
    HttpResponse::Ok()
        .content_type("text/html; charset: utf-8")
        .body(body)
}

async fn thumbnails(id: Identity) -> HttpResponse {
    println!("{:?}", id.identity());
    if id.identity().is_none() {
        return web_users::login_form_redirect("/thumbnails").await;
    }
    let mut body = Vec::new();
    match File::open("templates/thumbnails.html") {
        Ok(mut file) => {
            file.read_to_end(&mut body).unwrap();
        }
        Err(e) => {
            eprintln!("{} thumbnails.html", e);
        }
    }
    HttpResponse::Ok()
        .content_type("text/html; charset: utf-8")
        .body(body)
}

async fn wasm() -> HttpResponse {
    let mut body = Vec::new();
    match File::open("../wasm-home/pkg/wasm_home.js") {
        Ok(mut file) => {
            file.read_to_end(&mut body).unwrap();
        }
        Err(e) => {
            eprintln!("{} on 'wasm'", e);
        }
    }
    HttpResponse::Ok()
        .content_type("text/javascript")
        .body(body)
}

async fn boostrap_wasm() -> HttpResponse {
    HttpResponse::Ok()
        .content_type("text/javascript; charset: utf-8")
        .body(r#"import("/index.js");"#)
}

async fn index_js() -> HttpResponse {
    HttpResponse::Ok()
        .content_type("text/javascript; charset: utf-8")
        .body(r#"import * as wasm from "/wasm";"#)
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    let args = Args::from_args();
    let mut dest = dirs::data_dir().unwrap();
    dest.push("homecloud");
    std::fs::create_dir(&dest).unwrap_or(());
    let mut address = args.address;
    let tls = match args.keys {
		Some(mut keys) => {
			let mut keys = rsa_private_keys(&mut keys).unwrap();
			let mut config = ServerConfig::new(NoClientAuth::new());
			if let Some(mut cert) = args.cert {
				let chain = certs(&mut cert).unwrap();
				config.set_single_cert(chain, keys.remove(0)).unwrap();
			} else {
                eprintln!("When using TLS both key and cert has to be specified!");
                std::process::exit(1);
            }
			Some(config)
		}
		None => None
	};

    if let Some(interface) = &args.interface {
        for iface in pnet_datalink::interfaces() {
            if interface == &iface.name {
                for net in iface.ips {
                    if net.is_ipv4() {
                        address = Some(net.ip());
                    }
                }
                break;
            }
        }
    }

    let address = match address {
        Some(address) => {
            SocketAddr::new(address, args.port)
        }
        None => {
            println!("Network --interface or IP --address was not specified. Fallback to 127.0.0.1.");
            SocketAddr::new(std::net::IpAddr::from([127, 0, 0, 1]), args.port)
        }
    };

    let users = Users::load();
	let users = Arc::new(Mutex::new(users));
    let albums = Albums::load(PathBuf::from(args.path));
    let len = albums.albums.len();
	let media = Arc::new(Mutex::new(albums));
    if len == 0 {
        let media = media.clone();
        std::thread::spawn(move|| {
            let (tx, rx) = channel();
            let mut albums = media.lock().unwrap();
            // crawl with spawn X threads...
            // and put new albums in tx
            if let Err(err) = (*albums).crawl(tx) {
                eprintln!("{:?}", err);
                std::process::exit(1);
            }

            drop(albums);
            let mut len = 0;
            loop {
                match rx.recv() {
                    Ok(album) => {
                        let albums = media.lock();
                        match albums {
                            Ok(mut albums) => {
                                len += 1;
                                print!("\x1B[2K{} {}\r", len, album.name);
                                albums.insert(album);
                            }
                            Err(err) => {
                                eprintln!("Could not insert album {:?} to albums. {}", album.name, err);
                            }
                        }
                    }
                    Err(_) => {
                        /* recv has only one kind of an error and that is channel closed
                         * and that happens when crawl() goes out of scope. */
                        println!("\nCrawling is done. Found {} albums.", len);
                        let albums = media.lock();
                        if let Ok(mut albums) = albums {
                            albums.save();
                        }

                        break;
                    }
                }
            }
        });
    }


    std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::init();
    let server = HttpServer::new(move|| {
        App::new()
            .wrap(Logger::new("%r %{Content-Length}i %{User-Agent}i"))
            .wrap(IdentityService::new(
                    CookieIdentityPolicy::new(b"012345678901234567890123456789012")
                    .name("auth")
                    .path("/")
                    .max_age_time(chrono::Duration::minutes(30))
                    .secure(false), // this can only be true if you have https
                    ))
            .data(media.clone())
            .data(users.clone())
            .service(web::resource("/").to(index))
            .service(web::resource("/bootstrap.js").to(boostrap_wasm))
            .service(web::resource("/index.js").to(index_js))
            .service(web::resource("/stylesheet.css").to(stylesheet))
            .service(web::resource("/thumbnail/{id}").to(web_albums::download_thumb))
            .service(web::resource("/thumbnails").to(thumbnails))
            .service(web::resource("/download/{id}").to(web_albums::download_image))
            .service(web::resource("/album/list/{id}").to(web_albums::list_files))
            .service(web::resource("/albums/").to(web_albums::list_all))
            .service(web::resource("/wasm").to(wasm))
            .route("/user/login", web::get().to(web_users::login_form))
            .route("/user/login", web::post().to(web_users::login_submit))
            .route("/user/add", web::get().to(web_users::add_form))
            .route("/user/add", web::post().to(web_users::add_submit))
            .route("/album/add", web::get().to(web_albums::add_form))
            .route("/album/add", web::post().to(web_albums::add_submit))
    });

    println!("Server started. Listening on http://{} TLS: {}", address, tls.is_some());
    if let Some(tls) = tls {
        server.bind_rustls(&address, tls)?.run().await?;
	} else {
        server.bind(&address)?.run().await?;
	}

    Ok(())
}
