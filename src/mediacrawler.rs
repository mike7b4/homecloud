use std::io::{Write};
use std::sync::mpsc::{channel, Sender};
use std::sync::{Arc, Mutex};
use std::thread::JoinHandle;
use std::time::SystemTime;
use std::fmt;
use std::fs;
use std::path::PathBuf;
use std::fs::File;
use std::io::BufReader;
use dirs;
use chrono::{DateTime, offset::Utc};
use serde_json::json;
use serde::{Serialize, Deserialize};
use sha2::{Sha256, Digest};
use exif::{In, Tag};
use image;
#[derive(Debug)]
pub enum Error {
    IO(std::io::Error),
    Image(image::ImageError),
    CreateAlbum,
    AlbumAlreadyExists,
}

impl From<std::io::Error> for Error {
    fn from(err: std::io::Error) -> Error {
        Error::IO(err)
    }
}

impl From<image::ImageError> for Error {
    fn from(err: image::ImageError) -> Error {
        Error::Image(err)
    }
}


impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &*self {
            Error::IO(e) => write!(f, "{}", e),
            Error::Image(e) => write!(f, "Image {:?}.", e),
            Error::CreateAlbum => write!(f, "Create album failed."),
            Error::AlbumAlreadyExists => write!(f, "Album already exists."),
        }
    }
}

#[derive(Clone)]
struct Permission {
    permission: Vec<String>,
}

impl Permission {
    fn new() -> Self {
        Self {
            permission: Vec::new(),
        }
    }

    fn allowed() -> bool {
        true
    }
} 

impl Default for Permission {
    fn default() -> Self {
        Permission::new()
    }
}

enum Msg {
    Job(Job),
}

trait FnBox {
    fn call_box(self: Box<Self>);
}

impl<F: FnOnce()> FnBox for F {
    fn call_box(self: Box<F>) {
        (*self)()
    }
}

type Job = Box<dyn FnBox + Send + 'static>;

struct ThreadPool {
    threads: Vec<JoinHandle<()>>,
    sender: Sender<Msg>,
}

impl Default for ThreadPool {
    fn default() -> Self {
        let (tx, rx) = channel();
        let rx = Arc::new(Mutex::new(rx));
        let mut pool = Self { threads: Vec::with_capacity(10), sender: tx };
        for _ in 0..10 {
            let rx = rx.clone();
            let handle = std::thread::spawn(move|| {
                loop {
                    match rx.clone().lock().unwrap().recv() {
                        Ok(msg) => {
                            match msg {
                                Msg::Job(job) => {
                                    job.call_box();
                                }
                            }
                        }
                        _ => {}
                    }
                }
            });
            pool.threads.push(handle);
        }
        pool
    }
}

impl ThreadPool {
    fn push(&mut self, job: Job) {
        match self.sender.send(Msg::Job(job)) {
            Ok(_) => {}
            Err(e) =>  { eprintln!("{}", e); }
        }
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Image {
    id: String,
    #[serde(skip)]
    permission: Permission,
    file_name: String,
    title: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    datetime: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    xdimension: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    ydimension: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    exposure_time: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    f_number: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    focal_length: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    flash: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    copyright: Option<String>,
}

impl Image {
    fn new(path: PathBuf) -> Self {
        let datetime: DateTime<Utc> = fs::metadata(&path).unwrap().created().unwrap_or(SystemTime::UNIX_EPOCH).into();
        let datetime: String = datetime.format("%c").to_string();
        let file_name = String::from(path.file_name().unwrap().to_str().unwrap_or(""));
        let title = String::from(path.file_stem().unwrap().to_str().unwrap_or("")).replace("_", " ").replace("-", " ");
        let mut sha = Sha256::default();
        let mut id = String::with_capacity(64);
        sha.input(&path.to_str().unwrap().as_bytes());
        for byte in sha.result() {
            id += &format!("{:02x}", byte);
        }

        let mut f = Self {
            id,
            permission: Permission::new(),
            file_name,
            title,
            description: None,
            datetime: Some(datetime),
            xdimension: None,
            ydimension: None,
            exposure_time: None,
            f_number: None,
            copyright: None,
            focal_length: None,
            flash: None,
        };

        if let Err(_) = f.load_exif(&path) {
            //eprintln!("Could not read exif info for '{}'", &path.to_str().unwrap_or(""));
        }
        f
    }

    fn load_exif(&mut self, path: &PathBuf) -> Result<(), std::io::Error> {
        let file = File::open(path)?;
        match exif::Reader::new(&mut BufReader::new(&file)) {
            Ok(reader) => {
                let mut tags = [(Tag::ImageDescription, &mut self.description),
                                (Tag::DateTime, &mut self.datetime),
                                (Tag::PixelXDimension, &mut self.xdimension),
                                (Tag::PixelYDimension, &mut self.ydimension),
                                (Tag::ExposureTime, &mut self.exposure_time),
                                (Tag::FNumber, &mut self.f_number),
                                (Tag::FocalLength, &mut self.focal_length),
                                (Tag::Flash, &mut self.flash),
                                (Tag::Copyright, &mut self.copyright),
                ];

                for (tag, ref mut field) in tags.iter_mut() {
                    if let Some(value) = reader.get_field(tag.clone(), In::PRIMARY) {
                        **field = Some(value.display_value().with_unit(&reader).to_string());
                    }
                }
            }
            Err(e) => { // eprintln!("{} on '{:?}'", e, path)
            }
        }

        Ok(())
    }
}

impl fmt::Display for Image {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut s = format!("id: {}", self.id);
        s += &format!("file: {}\n", self.file_name);
        write!(f, "{}", s)
    }
}

#[derive(Serialize, Deserialize)]
pub struct Albums {
    id: String,
    pub name: String,
    #[serde(skip_serializing)]
    pub directory: PathBuf,
    // files in current directory
    #[serde(skip_serializing)]
    images: Vec<Image>,
    pub albums: Vec<Albums>,
    #[serde(skip)]
    pool: ThreadPool,
}

// Private
impl Albums {
    fn id(path: &PathBuf) -> String {
        let mut sha = Sha256::default();
        let mut id = String::with_capacity(64);
        sha.input(&path.to_str().unwrap().as_bytes());
        for byte in sha.result() {
            id += &format!("{:02x}", byte);
        }

        id
    }

    fn thumbnail(&self) -> Result<(), Error> {
        if self.images.len() == 0 {
            return Ok(());
        }
        let mut num = rand::random::<usize>() % self.images.len();
        if num == self.images.len() {
            num -= 1;
        }
        let mut src = self.directory.clone();
        src.push(&self.images[num].file_name); 
        let mut dest = dirs::data_dir().unwrap();
        dest.push(&format!("homecloud/{}.jpg", &self.id));
        if fs::metadata(&dest).is_ok() {
         //   println!("Thumb already created for '{:?}'", dest);
            return Ok(());
        }
        let thumb = image::open(src)?;
        let thumb = thumb.thumbnail(320, 200);
        let mut file = File::create(&dest)?;
        if let Err(err) = thumb.write_to(&mut file, image::JPEG) {
            eprintln!("Create thumbnail {:?} {}", dest, err);
        }
        Ok(())
    }
}
 
impl Albums {
    pub fn new(path: PathBuf) -> Self { 
        Self {
            id: Albums::id(&path),
            name: String::from(path.file_name().unwrap().to_str().unwrap_or("?")),
            directory: path,
            images: Vec::new(),
            albums: Vec::new(),
            pool: ThreadPool::default(),
        }
    }

    pub fn create_album(&mut self, name: &str) -> Result<(), Error> {
        if name.find("/").is_some() {
            return Err(Error::CreateAlbum);
        }

        let mut path = self.directory.clone();
        path.push(name);
        if path.exists() {
            return Err(Error::AlbumAlreadyExists);
        }

        fs::create_dir(&path).map_err(|e| {
            eprintln!("{}", e);
            Error::CreateAlbum
        })?;

        self.insert(Albums::new(path));
        self.save();
        Ok(())
    }

    pub fn crawl(&mut self, tx: Sender<Albums>) -> Result<(), Error> {
        for entry in fs::read_dir(&self.directory)? {
            let dir = entry?;
            if dir.metadata()?.is_dir() {
                let tx = tx.clone();
                self.pool.push(Box::new(move|| {
                    let mut dir = Albums::new(dir.path());
                    match dir.crawl(tx.clone()) {
                        Ok(()) => {
                            tx.send(dir).map_err(|e|{ eprintln!("{}", e); } ).unwrap();
                        }
                        Err(e) => {
                            eprintln!("{:?}", e);
                        }
                    };
                }));
           } else {
                match dir.path().extension() {
                    Some(ext) => match &ext.to_str().unwrap_or("") {
                        &"jpg" | &"JPG" | &"jpeg" | &"JPEG" => {
                            let image = Image::new(dir.path());
                            self.images.push(image);
                        }
                        _ => {}
                    }
                    None => {}
                }
            }
        }

        if let Err(e) = self.thumbnail() {
            eprintln!("{}", e);
        }
        self.images.sort_by(|a, b| a.file_name.cmp(&b.file_name));
        Ok(())
    }

    pub fn insert(&mut self, album: Albums) {
        for (i, ref a) in self.albums.iter().enumerate() {
            if album.name > a.name {
                self.albums.insert(i, album);
                return ;
            }
        }

        self.albums.push(album);
    }

    pub fn albums(&self, id: &str) -> serde_json::Value {
        let mut dir = Some(self);
        if id.len() > 0 {
            for album in &self.albums {
                if album.id == id {
                    dir = Some(album);
                    break ;
                }
            }
        }

        return json!(dir);
    }

    pub fn album(&self, id: &str) -> Option<serde_json::Value> {
        if id.len() == 0 {
            return Some(json!(&self.images));
        }

        for album in &self.albums {
            if album.id == id {
                return Some(json!(album.images));
            }
        }

        None
    }

    // Lookup file
    pub fn thumbnail_path(&self, id: &str) -> Option<PathBuf> {
        if id == self.id {
            let mut path = dirs::data_dir().unwrap();
            path.push("homecloud");
            path.push(format!("{}.jpg", &id));
            return Some(path);
        }
        for album in &self.albums {
            if let Some(path) = album.thumbnail_path(id) {
                return Some(path);
            }
        }

        None
    }

 
    // Lookup file
    pub fn image_path(&self, id: &str) -> Option<PathBuf> {
        for image in &self.images {
            if image.id == id {
                let mut path = self.directory.clone();
                path.push(image.file_name.clone());
                return Some(path);
            }
        }

        // Search all albums to lookup file
        for album in &self.albums {
            if let Some(file) = album.image_path(&id) {
                return Some(file);
            }
        }

        None
    }

    pub fn save(&mut self) {
        if self.albums.len() == 0 {
            return ;
        }
        let mut file = dirs::config_dir().unwrap();
        file.push(&format!("homecloud/{}.json", &self.id));
        println!("Save: {:?} as {}.json", self.directory, self.id);
        match File::create(file) {
            Ok(mut file) => {
                let mut js = json!(self);
                /* This fields are normally skipped when serialize
                 * so we need to insert them. */
                js["directory"] = json!(self.directory);
                js["images"] = json!(self.images);
                for (i, ref album) in self.albums.iter().enumerate() {
                    js["albums"][i]["directory"] = json!(album.directory);
                    js["albums"][i]["images"] = json!(album.images);
                }
                if let Err(err) = file.write(&serde_json::to_string(&js).unwrap().as_bytes()) {
                    eprintln!("Bailed with: {} on '{:?}'", err, file);
                }
            }
            Err(e) => {
                println!("{} on '{}'", e, self.directory.to_str().unwrap_or(""));
            }
        }
    }

    pub fn load(path: PathBuf) -> Self {
        let id = Albums::id(&path);
        let mut conf = dirs::config_dir().unwrap();
        conf.push(&format!("homecloud/{}.json", &id));
        match File::open(conf) {
            Ok(file) => {
                print!("Load from: '{}.json' please wait...", &id);
                std::io::stdout().flush().unwrap();
                let reader = BufReader::new(file);
                let album: serde_json::Result<Albums> = serde_json::from_reader(reader);
                match album {
                    Ok(album) => {
                        println!(" OK");
                        album
                    }
                    Err(e) => {
                        eprintln!("{:?}", e);
                        Albums::new(path)
                    }
                }
            }
            Err(e) => {
                eprintln!("{:?} on '{}.json' rescan directories.", e, id);
                Albums::new(path)
            }
        }
    }
}

impl Drop for Albums {
    fn drop(&mut self) {
        self.save()
    }
}

