use std::fs::File;
use std::io::Read;
use std::sync::{Arc, Mutex};
use serde::Deserialize;
use crate::mediacrawler::Albums;
use actix_files::NamedFile;
use actix_web::{HttpResponse, web};
use crate::JsonResponse;

#[derive(Deserialize)]
pub struct FormAlbum {
    name: String,
}

pub async fn download_thumb(id: web::Path<String>, albums: web::Data<Arc<Mutex<Albums>>>) -> Result<NamedFile, HttpResponse> {
    let ref albums = *(albums.lock().unwrap());
    println!("{}", id);
    let file = albums.thumbnail_path(&id);
    drop(albums);
    if let Some(file) = file {
        return NamedFile::open(&file).map_err(|e| { eprintln!("{}", e); HttpResponse::NotFound().finish() });
    }

    Err(HttpResponse::NotFound().finish())
}

pub async fn download_image(id: web::Path<String>, albums: web::Data<Arc<Mutex<Albums>>>) -> Result<NamedFile, HttpResponse> {
    println!("id {}", id);
    let ref albums = *(albums.lock().unwrap());
    let file = albums.image_path(&id);
    drop(albums);
    if let Some(file) = file {
        return NamedFile::open(&file).map_err(|_| { HttpResponse::NotFound().finish()});
    }

    Err(HttpResponse::NotFound().finish())
}

async fn js_albums(id: &str, albums: web::Data<Arc<Mutex<Albums>>>) -> HttpResponse {
    let ref albums = *(albums.lock().unwrap());
	HttpResponse::Ok().json(albums.albums(id))
}

pub async fn list_all(albums: web::Data<Arc<Mutex<Albums>>>) -> HttpResponse {
    js_albums("", albums).await
}

pub async fn list_files(id: web::Path<String>, albums: web::Data<Arc<Mutex<Albums>>>) -> HttpResponse {
    let ref albums = *(albums.lock().unwrap());
    if let Some(js) = albums.album(&id) {
        return HttpResponse::Ok().json(&js);
    }
    HttpResponse::NotFound().finish()
}

pub async fn add_submit(form: web::Json<FormAlbum>, albums: web::Data<Arc<Mutex<Albums>>>) -> HttpResponse {
    if let Err(e) = (*albums.lock().unwrap()).create_album(&form.name) {
        return HttpResponse::Ok().json(JsonResponse::new(&format!("{}", e), ""));
    }

    return HttpResponse::Ok().json(JsonResponse::new("Album was added", "/"));
}

pub async fn add_form() -> HttpResponse {
    let mut body = Vec::new();
    match File::open("templates/album_add.html") {
        Ok(mut file) => {
            file.read_to_end(&mut body).unwrap();
        }
        Err(e) => {
            eprintln!("{} album_add.html", e);
        }
    }
    HttpResponse::Ok()
        .content_type("text/html; charset: utf-8")
        .body(body)
}
