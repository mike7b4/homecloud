# Homecloud

Just a simple POC of an "local" imageviewer and show it in your web browser.
I hope to expand this to be a simple "nextcloud solution" without hassle to setup a db and so on.

# Dependies

For full dependies see Cargo.toml:

 - actix-web >= 2.0
 - tokio
 - rustls
 - structopt
 - pnet_datalink
 - serde_json
 - kamadak-exif
 - Rust >= v1.40

# How to run it

```cargo run -- --media <PATH_TO_YOUR_LOCAL_PHOTOS>```

Then point your browser to localhost:4444/image/

# Todo

 - [X] Bind to a network interface instead of localhost.

 - [X] Create a new album.

 - [ ] Add authentication mechanism.

 - [ ] Create image thumbnails.

 - [ ] Make a cooler Web UI and stylesheet.

 - [ ] rescan album path.
